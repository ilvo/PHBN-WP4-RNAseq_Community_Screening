## Detailed analysis: Kraken2 against Genbank Nucleotide nr
### 0. Used software
- [Kraken2](https://github.com/DerrickWood/kraken2/wiki)
- [Krona Import taxonomy](https://github.com/marbl/Krona/wiki)


### 1. Introduction
This pipeline is used for Illumina RNA-seq data. It is designed to detect all kinds of organisms in the sample. The reads are directly classified using Kraken2 and visualized in Krona.

### 2. Kraken2
The reads are taxonomically classified using Kraken2 against a local copy of the Genbank Nucleotide nr database. To be able to use this database, you first need to create a Kraken2 database from it, which is computationally very demanding. See [Kraken2 help](https://github.com/DerrickWood/kraken2/wiki/Manual#custom-databases) for more details on how to create this custom database.

`kraken2 <options> --db <path to the local nr database in kraken2 format> --output <output file name> --report <report file name> <input file 1> <input file2>`

The following options can be used in Kraken2:
- `--db \<Path\>`: Path to database
- `--output \<file name\>`: file name for output file
- `--report \<file name\>`: file name for report file
- `--threads \<number\>`:  Number of threads
- `--paired \<number\>`: To use paired data
- `--report-zero-counts`:  Report all sequences in the database in the output file, even if they have 0 counts
- `--memory-mapping`: avoid loading database in to RAM
- `--gzip-compressed`: input files are compressed with gzip
- `--classified-out \<file name\>`: print classified sequences to a seperate file

Other options can be found at the help page of Kraken2 (`kraken2 --help`).

>*For example:*<br>
`kraken2 --db /home/genomics/bioinf_databases/kraken2/NCBI_nt --paired --report-zero-counts --gzip-compressed --memory-mapping --threads 36 --classified-out kraken2_classified#.fastq --output kraken2_output.txt --report kraken2_report.txt sample_F_trimmed.fastq.gz sample_R_trimmed.fastq.gz`

### 3. Visualization
Herefore Krona can be used on the file created at the Data transformation step.
In the example below, we use the contig lengths as magnitude in the Krona plot.

`ImportTaxonomy.pl -q 2 -t 3 <Kraken2 output txt file>`

The following options can be used in Krona:
- `-o \<filename\>`: Output file name. [Default: 'taxonomy.krona.html']
- `-t \<number\>`: Column of input files to use as taxonomy ID [Default: '2']
- `-q \<number\>`: Column of input files to use as query ID. Required if magnitude files are specified [Default: '1']
Other options can be found at the help page of Krona (`ImportTaxonomy`).

>*For example:*<br>
`ImportTaxonomy.pl -tax /usr/share/bioinf_databases/Krona/taxonomy -q 2 -t 3 -o krakenoutput.html kraken2_output.txt`
