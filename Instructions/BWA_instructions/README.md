# Instructions on how to map using BWA and samtools

## Prerequisites
A computer operating under a Linux system with [`BWA`](http://bio-bwa.sourceforge.net/) and [`samtools`](https://github.com/samtools/samtools) installed.

## Introduction
This document describes how you can map your reads against the provided database using the software `BWA`. After mapping, the software `samtools` is used to extract the number of reads that mapped against each reference sequence.

## Database preparation
First, download the [database](https://gitlab.com/ilvo/PHBN-WP4-RNAseq_Community_Screening/-/tree/master/Instructions/SILVA_132_LSU_database).
Next, using command line, navigate to the folder where your database is and create the necessary database index files as follows:
`bwa index SILVA_132_LSURef_tax_silva_trunc_renamed.fasta`

This may take a while, so be patient.

## Mapping with BWA
Below, examples are given on how you can map your reads (either single end or paired end) against the downloaded database using `BWA`.

#### Mapping single end reads

The command is built as follows:
`bwa mem -t <threads> databasefile readsfile | samtools sort -@ <threads> - > outputfile`

The `bwa mem` command takes as first argument the database in `fasta` format and as second argument the single reads file (in `fastq` or `fastq.gz` format). You can add the option `-t` with the numbers of threads (processors) you want to use during the mapping.
The result is immediately piped into the `samtools sort` command to sort the reads according to coordinate. Also here, you can add the option `-@` with the numbers of threads (processors) you want to use during the sorting process.
The sorted mapping result is written to an output file in `BAM` format.

Example:
`bwa mem -t 8 /home/ahaegeman/PHBN_WP4/SILVA_132_LSURef_tax_silva_trunc_renamed.fasta sample1_R1_001.fastq | samtools sort -@ 8 - > sample1_LSU_sorted.bam`


#### Mapping paired end reads

The command is built as follows:
`bwa mem -t <threads> databasefile forwardreadsfile reversereadsfile | samtools sort -@ <threads> - > outputfile`

The `bwa mem` command takes as first argument the database in `fasta` format, as second argument the forward reads file (in `fastq` or `fastq.gz` format) and as third argument the reverse read file (in `fastq` or `fastq.gz` format). You can add the option `-t` with the numbers of threads (processors) you want to use  during the mapping.
The result is immediately piped into the `samtools sort` command to sort the reads according to coordinate. Also here, you can add the option `-@` with the numbers of threads (processors) you want to use during the sorting process.
The sorted mapping result is written to an output file in `BAM` format.

Example:
`bwa mem -t 8 /home/ahaegeman/PHBN_WP4/SILVA_132_LSURef_tax_silva_trunc_renamed.fasta sample1_R1_001.fastq sample1_R2_001.fastq | samtools sort -@ 8 - > sample1_LSU_sorted.bam`

## Process mapping results using samtools
After the mapping, we use `samtools idxstats` to get some statistics, in our case the number of reads which mapped against each sequence.

First we need to make an index file of our mapping file using `samtools index`.

`samtools index sample1_LSU_sorted.bam`

Next, we can calculate the statistics with `samtools idxstats` and save the output to a new file, here named "sample1_LSU_idxstats.txt".

`samtools idxstats sample1_LSU_sorted.bam > sample1_LSU_idxstats.txt`

The output is a simple TAB-delimited text file with each line consisting of the reference sequence name, the reference sequence length, number of mapped reads and number of unmapped reads.

You have finished the analysis! The resulting file can be uploaded into the [Result_folder](https://drive.google.com/drive/folders/1p_I2fY5kzFpLFX09-gMhT5sdk5ge3CFj?usp=sharing) of the Google Drive. Don't forget to complete a [Metadata file](https://docs.google.com/spreadsheets/d/1uFWrBK5xfABpAMEdoLGJirl60Qw2naTaN_WHPDzApSI/edit?usp=sharing) for your sample(s) and upload that file to the [Metadata_folder](https://drive.google.com/drive/folders/1jQMeO_h2D5s6bY-QSQL5Tfb_FDqkSYjW?usp=sharing).

## Bonus: in case you are curious about your results :-)

Using some simple command line tricks you can quickly scan the `samtools idxstats` output for interesting results yourself. Here are some examples:

What are the top 10 sequences with the most reads mapped?
`sort -k3 -n -r sample1_LSU_idxstats.txt | head -n 10`

What are the top 10 sequences with the most reads mapped, except for Chloroplasts and Mitochondria?
`grep -v 'Chloroplast' sample1_LSU_idxstats.txt | grep -v 'Mitochondria' | sort -k3 -n -r | head -n 10`

How many sequences have more than 5000 mapped reads?
`awk '$3>5000' sample1_LSU_idxstats.txt | wc -l`

What are the top 10 sequences with the most reads mapped belonging to Fungi?
`awk -F, '$2=="Fungi" {print $0}' samtools_idxstats_"$SAMPLE"_LSU_sorted.txt | sort -k3 -n -r | head -n 10`

Replace `Fungi` in the previous command with `Phytoplasms`, `Bacteria`, `Plants`, `Nematodes`, `Oomycetes`, `Insects` or `SpidersAndMites` to look for those categories.

Remember to always check the number of reads which map to the sequence! Very few mapping reads = unreliable result. Also remember that we are mapping against a rRNA database, and many artefacts can be present (low mapping quality, low similarity, errors in SILVA database, missing data in SILVA database, etc.). More detailed analysis is always necessary to confirm the presence of a certain organism.

Have fun exploring!
