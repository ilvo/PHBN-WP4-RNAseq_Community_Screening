## MAPPING IN CLC
-------------------------

### 1. Importing read files and SILVA database

#### 1.1 Create new folder
Richt click on the local or another existing folder.
To create a new folder click on **New --> Folder** (Ctrl+Shift-N)

![](images/CreateNewFolder.PNG)

#### 1.2 Import read files
To import read files (e.g. merged reads) click in the menu **-->Import --> Illumina...**

![](images/ImportReadsFile.PNG)

* Choose which file you want to run and click --> **Next**
* Don't change the other options
* **Save** Result handling
* Enter location where to save the file

Download the custom SILVA database from [here](https://gitlab.com/ilvo/PHBN-WP4-RNAseq_Community_Screening/-/tree/master/Instructions/SILVA_132_LSU_database).
To import fasta SILVA database click on menu **-->Import -->Fasta Read Files...**

![](images/ImportSilvaDatabase.PNG)

* Click **Next** and **Save**
* Enter locaton where to save the file

### 2. Map reads to SILVA database
First select the readfile to which you want to map.
Select Toolbox in the menu **--> Resequencing analysis --> Map reads to reference...**

![](images/MapReadsToReference.PNG)

* **Select sequencing reads** (e.g. merged) and click on **Next.** <br/>

![](images/SelectSequencingReads.PNG)

* **Select reference** (SILVA database) and click on **Next.** <br/>

![](images/SelectReference.PNG)

* Make sure that in the mapping options **Map randomly** is checked. <br/>
* **Create stand-alone read mappings** in the output options of result handling. <br/>

![](images/CreateStandAloneReadMappings.PNG)

* Click on **Next**
* Save file in the correct outputfolder and click on **Finish.**

### 3. Export mapping results
The mapping results are in table-form. If you double click you see an output as:

![](images/OutputFileMapping.PNG)

* Right click on the mapping table (results of mapping) in the existing folder and click on **Export...**

![](images/ExportMappingFile.PNG)

* Select **Tab delimited text** as export format.

![](images/SelectExportFormat.PNG)

* Select the mapping table and click on **Next**.
* Make sure that the option *Export all collumns* is unchecked!
* Click on **Next**.

![](images/SpecifyExportParameters.PNG)

* Select 3 columns for export:
✔ Name
✔ Total read count
✔ Reference length

![](images/SelectColumnsForExport.PNG)
* Click **Next**.
* Choose the correct foldername for the export textfile (.txt)
* **Finish**

You can **upload this file** to the *Result_files subfolder in the [WP4 - RNAseq community effort](https://drive.google.com/drive/folders/1Wyp-jJV_7m9TKKnWOjnYevfWR1S1Agma?usp=sharing) Google drive PHBN folder.* Don't forget to also complete the **metadata** [template](https://drive.google.com/open?id=1uFWrBK5xfABpAMEdoLGJirl60Qw2naTaN_WHPDzApSI) with some extra info on the samples you analyzed.

*In case you want to explore the results yourself, you can open Tab delimited textfile in for example Notepad++ and copy it to Excel to view the results in separated columns. If you extract more columns to the exportfile, you will have more information on the mapped reads for further analysis.*
