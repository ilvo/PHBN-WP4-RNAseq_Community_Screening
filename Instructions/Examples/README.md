# Examples

## Sample info

Three publicly available RNA-seq datasets (2x150 bp) derived from plant material are used in this example, see table below.

| Sample 	| Plant         	| Virus detected 	| Publication                                                            	|
|--------	|---------------	|----------------	|------------------------------------------------------------------------	|
| 1      	| *Ribes* spp.    	| BcLRaV-1       	| [Koloniuk et al., 2018](https://doi.org/10.3390/v10070369)             	|
| 2      	| *Pleione* spp.  	| PlFBV          	| [Kominek et al., 2019](https://doi.org/10.1016/j.virusres.2018.12.009) 	|
| 3      	| *Pistacia* spp. 	| PiVB           	| [Buzkan et al., 2019](https://doi.org/10.1016/j.virusres.2019.01.012)  	|

## Mapping against SILVA 132 LSU

The reads were mapped against the SILVA v132 LSU database using [BWA](http://bio-bwa.sourceforge.net/).

Next, for each organism group of interest, the number of mapped reads were summed and are shown in the barplot below.


![](images/ULG_rRNA_LSU_counts_sample1-3.png)

This can also be shown as a percentage (% of mapped reads belonging to a certain organism group). In this case, also the category "Other" is added, i.e. reads which mapped to rRNA of the database, but not belonging to the organism groups of interest.

![](images/ULG_rRNA_LSU_perc_sample1-3.png)


We can see that all samples have a relatively large amount of rRNA sequences, since the total number of mapped reads ranges from roungly 800.000 (Sample3) to over 2.500.000 (Sample1). Most of the rRNA is derived from the plant material. **Sample3 has the largest proportion of non plant rRNA** (roughly 10% of the rRNA). This is mainly derived from the groups Bacteria, Insects and SpidersAndMites.

When we look at the output of the mapping itself, i.e. the number of reads mapped to each individual sequence, we can look up which organisms are most prevalent for each category.

For Sample3 we check which **Bacteria** rRNA sequence was most mapped against (861 reads):
`CDAZ01000009.1611.4652,Bacteria,Bacteria;Proteobacteria;Gammaproteobacteria;Pseudomonadales;Pseudomonadaceae;Pseudomonas;Pseudomonas_aeruginosa`
*Pseudomonas aeruginosa* is a common bacterium present in soil and water, and also in plants. This bacterium probably lives there as a harmless endophyte. In some cases, *P. aeruginosa* can become an opportunistic pathogen. It could also be another *Pseudomonas* species, species from this genus are common plant endophytes.

We also check which **Insects** rRNA sequence was most mapped against (3600 reads):
`MTTC01001356.11867.14434,Insects,Eukaryota;Opisthokonta;Holozoa;Metazoa_(Animalia);Eumetazoa;Bilateria;Arthropoda;Hexapoda;Insecta;Pterygota;Drosophila_serrata`
This data suggest that the plant material contained some genetic material from a dipteran insect (e.g. eggs). Chances are however small that the species identification is correct, since Drosophila serrata is endemic to Australia, and does not occur in the region where the sample came from (Turkey). Likely, another *Drosophila* or an insect from a closely related genus is the true species. The fact that the sequences mapped to this sequence does not mean that this organism is present, it rather means that this sequence was the closest match in the reference database used (SILVA v132 in this case).

Finally, we also check which **SpidersAndMites** rRNA sequence was most mapped against (5424 reads):
`JQ000582.1.3639,SpidersAndMites,Eukaryota;Opisthokonta;Holozoa;Metazoa_(Animalia);Eumetazoa;Bilateria;Arthropoda;Chelicerata;Arachnida;Alloptes_obtusolobus`
This data suggest that the plant material contained some genetic material from a mite (e.g. eggs). Chances are however small that the species identification is correct, since again this species is endemic to Australia. Likely, another mite from a closely related species is the true species.

In **conclusion**, the rRNA mapping strategy can give us an idea on how many rRNA reads that there are present in our data, and from which organism groups that these are likely derived. We should however not trust species assignments since the data can be biased due to the relatively short sequence we are looking at (LSU rRNA), and to the completeness of the rRNA database.

## Detailed metagenomic analysis using Kraken2

The software [Kraken2](https://ccb.jhu.edu/software/kraken2/) is a taxonomic classification system using exact k-mer matches to achieve high accuracy and fast classification speeds. This classifier matches each k-mer within a query sequence to the lowest common ancestor (LCA) of all genomes containing the given k-mer. It was recently benchmarked against many other available metagenomics taxonomic classifiers, and it performed among the best in terms of accuracy and required computational resources ([Simon et al., 2019](https://doi.org/10.1016/j.cell.2019.07.010)). Nevertheless, it still requires quite some computational power (especially to make custom databases), and it runs by command line on Linux systems, which is a hurdle for many scientists to include it in their analysis.

Here, we analyzed the three samples using the Kraken2 software, using the entire Genbank nonredundant Nucleotide database as reference.

The Kraken2 taxonomic assignment were again summed per organism group.

![](images/ULG_kraken_counts_sample1-3.png)
![](images/ULG_kraken_perc_sample1-3.png)

From the figures, it can be seen that the global sample composition is roughly the same for the organism categories as was observed in the mapping against LSU rRNA. Only in the Kraken2 analysis, more reads are assigned to each group (since it considers all RNA, not only rRNA), and the group "Other" is much larger. This is because in this case, the group also contains the "unassigned" sequences. Furthermore, the "Other" group also contains the viruses. In case of the mapping approach against the SILVA LSU rRNA database, we were blind for viruses since these do not have rRNA and are hence not in the rRNA database.

The complete composition of Sample 3 can be visualized as a pie chart. We can see that about 25% of the data is unclassified, 68% is derived from plants, 1% from bacteria and 1% from the virus we expected to be present.

![](images/Sample3_kraken_screenshot_full.png)

If we want to explore the Arachnida, which contains the mites, we can make a new pie chart only showing the Arachnida (about 0.4% of the data). In the plot you can see that almost all sequences are classified as being part of the Eriophyidae, which is a family of gall mites, often causing damage to the plant. The Eriophyidae reads are 0.4% of all sequenced reads in the sample, which does not seem like a lot, but in absolute numbers it concerns of 18832 reads, which is definitely not neglectable.

![](images/Sample3_kraken_screenshot_Arachnida.png)

If you want to explore the results in more detail yourself, you can download the corresponding html files to your local computer and open them in your favorite web browser (for example Google Chrome). These are interactive [Krona](https://github.com/marbl/Krona/wiki) plots, where you can double click each taxonomic group of interest to zoom in.

[Download link to results of sample1](https://gitlab.com/ilvo/PHBN-WP4-RNAseq_Community_Screening/-/blob/master/Instructions/Examples/images/Sample1_kraken.html)

[Download link to results of sample2](https://gitlab.com/ilvo/PHBN-WP4-RNAseq_Community_Screening/-/blob/master/Instructions/Examples/images/Sample2_kraken.html)

[Download link to results of sample3](https://gitlab.com/ilvo/PHBN-WP4-RNAseq_Community_Screening/-/blob/master/Instructions/Examples/images/Sample3_kraken.html)


## Further analyses

It is clear that definitely some potentially interesting non-viral organisms are present in the plant material. To investigate this further, more in detail analysis is necessary for specific organism groups. One of the approaches could be to extract all reads from a ceratin organism group (as classified by Kraken2), and do a meta-assembly on these reads (for example mites). Then the longer contigs can be again studied in more detail by for example `blastn`. Also, mappings could be done on to specific reference genomes from (closely related) species suspected to be present in the sample. These analyses can be done in close collaboration with the researcher who initially analyzed the sample, to discuss which organisms are likely the most interesting.
