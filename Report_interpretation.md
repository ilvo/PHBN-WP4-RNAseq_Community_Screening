## Interpretation of report

### First phase: mapping against SILVA database

#### Introduction
In the first phase of the analysis, RNA reads were mapped against the [SILVA LSU rRNA database (v132)](https://www.arb-silva.de/). The LSU database was chosen because in preliminary mapping experiments, mapping against LSU reflected the actual composition of the reads better than the SSU database.
We used this analysis as a "proxy" to select some potentially interesting samples for further analysis. The mapping approach had as advantages that it was relatively quick, participants could use their favorite mapping strategy, and no raw data had to be transferred (only mapping reports).

The headers from the sequences of the SILVA LSU database were transformed to include an organism category: plants, insects, spiders and mites, bacteria, fungi, phytoplasms, oomycetes and others. This category was based on the taxonomic classification from SILVA. The mapping report, which includes the number of mapped reads against each sequence, allows us to make a rough estimation of the presence of each of the organism categories in the sample.

#### Figures

The figures show the **percentage of mapped reads** for each organism category (as opposed to the total number of mapped reads). You can hence interprete the figure as a **rough estimation of the composition of the rRNA pool** of your samples.

#### Some important remarks

- The majority of your sample should have a plant origin. If this is not the case, your sample might have been (severely) contaminated.
- We observed that in some cases, the category Fungi is overestimated because of taxonomic classification errors in SILVA. This means that the sequence was actually plant-derived, but had a (wrong) taxonomic classification in the Fungi kingdom, and is hence misclassified.
- We are blind for viruses/viroids since these do not have rRNA and are hence not represented in SILVA.
- Classification of the reads is very dependent on the completeness of the rRNA database. If the actual organism present in your sample is not present in the database, and if there are also no close relatives present, there might be misclassifications (for example insect reads which are classified as nematode reads). Moreover, we are using the reads directly for mapping without assembly, these short reads can sometimes map to multiple organisms with the same score. The results should hence always be handled with care.


### Second phase: detailed taxonomic classification

#### Introduction

In the detailed analysis, reads were directly taxonomically classified (using the k-mer based approach [Kraken2](https://github.com/DerrickWood/kraken2/wiki)) to the non-redundant Genbank Nucleotide database (nr). The results were visualized using an interactive [Krona](https://github.com/marbl/Krona/wiki) plot.

TThe Krona plot is an interactive **HTML** file. After saving this file on your computer, you can open it using for example [Google Chrome](https://www.google.com/intl/nl/chrome/).

The pie chart shows the abundance of different taxonomic groups across the reads. The size of each pie chart chunk is dependent on the number of reads associated with that taxon (the so-called **Count**). For example, if 5000 reads were found to be classified with species X, and 1000 other reads are classified to species Y; the pie chunk of species X will be 5x larger than species Y.

Note that we did not do any preprocessing of the reads (except for adapter removal), this means that if there are lots of duplicate reads resulting from too many PCR cycles during library preparation, the abundance of some taxa might be overestimated.

**Double clicking** a taxon will show you a new, zoomed-in version of the pie chart, now only for that taxon. On the top right in the legend, you can check what percentage this taxon is from the root (in terms of relative number of reads). 

#### Some important remarks
- **Classification of the reads is very dependent on the completeness of the database.** If the sequence corresponding to the sequence present in your sample is not present in the database, and if there are also no close relatives present, there might be severe misclassifications (for example insect sequences which are classified as from nematode origin). The results should hence always be handled with care, and you should check the average score to evaluate the reliability of the taxon classification.
- **The presence of an organism in the data does not automatically mean it is present in your plant sample**, it could also be from contamination. Also, when it's a pathogen, you should always check whether this pathogen is pathogenic on your host plant, since often organisms only have a narrow host range. It could also be that a close relative of the pathogen is a normal non-pathogenic endophyte of your plant, but due to overrepresentation of sequences from pathogens in the sequence databases, it is classified as a pathogen after all.
