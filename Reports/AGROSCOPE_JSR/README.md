## AGROSCOPE_JSR results

### Participant information

| Name             	| Email                 	| Institute 	| Country 	|
|------------------	|-----------------------	|-----------	|---------	|
|  Jean-Sébastien Reynard | jean-sebastien.reynard@agroscope.admin.ch 	|   AGS 	| Switzerland 	|


### Sample info

|  Sample name | Original name  | Plant species  | Tissue  | Viruses/viroids detected in previous analyses  | Read length  | Mapping method  |  Selected for detailed analysis? |
|---|---|---|---|---|---|---|---|
|  Agroscope_JSR_Vitis_S1 | Agroscope_HYF12  | *Vitis vinifera*  | branches  | GRBV, GRSPaV, HSVd  | 1 x 150bp  |  BWA | no |
| Agroscope_JSR_Vitis_S2  | Agroscope_HYF24  | *Vitis vinifera*  |  leaves |  GLRaV-3, GLRaV-4, GRSPaV, HSVd | 1 x 150bp  | BWA  | no |

### Results first phase: mapping against SILVA 132 LSU

The reads were mapped against the SILVA v132 LSU database as explained in the [instructions](../../Instructions).
Mapping reports were sent to ILVO, and summarizing graphs were made.


For each organism group of interest, the number of mapped reads were summed and are shown in the barplot below. The percentage in the Y-axis points to the proportion of mapped reads as compared to the total number of mapped reads. You can hence interprete the complete bar as the total rRNA population of a sample, colored by organism category. The category "Other" points to reads which mapped to the rRNA database, but do not belong to the organism groups of interest (Bacteria, Fungi, Insects, SpidersAndMites, Nematodes, Oomycetes, Phytoplasms, Plants).

![AGROSCOPE_JSR_overview_rRNAcontent](./images/AGROSCOPE_JSR_overview_rRNAcontent.png)

In the graph below, the same barplot is shown, but now without the categories "Plants" and "Other", showing a "zoomed-in" version of the previous barplot.

![AGROSCOPE_JSR_overview_rRNAcontent_withoutplants](./images/AGROSCOPE_JSR_overview_rRNAcontent_withoutplants.png)

### None of the samples were selected for futher analysis.
