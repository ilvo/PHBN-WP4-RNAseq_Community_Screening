## AGROSCOPE_OS results

### Participant information

| Name             	| Email                 	| Institute 	| Country 	|
|------------------	|-----------------------	|-----------	|---------	|
| Olivier Schumpp 	| olivier.schumpp@agroscope.admin.ch 	|  AGS  	| Switzerland 	|

### Sample info

| Sample name  | Original name  |  Plant species | Tissue  |  Viruses/viroids detected in previous analyses  | Read length |  Mapping method | Selected for detailed analysis? |
|---|---|---|---|---|---|---|---|
| Agroscope_OS_Solanum_S1 | Agroscope_HXU-2 | *Solanum tuberosum*  | tubers | PVY, PLRV, PVX, PVS, PMTV, ArMV, SLRV  | 2 x 125bp  | BWA  | no |

### Results first phase: mapping against SILVA 132 LSU

The reads were mapped against the SILVA v132 LSU database as explained in the [instructions](../../Instructions).
Mapping reports were sent to ILVO, and summarizing graphs were made.


For each organism group of interest, the number of mapped reads were summed and are shown in the barplot below. The percentage in the Y-axis points to the proportion of mapped reads as compared to the total number of mapped reads. You can hence interprete the complete bar as the total rRNA population of a sample, colored by organism category. The category "Other" points to reads which mapped to the rRNA database, but do not belong to the organism groups of interest (Bacteria, Fungi, Insects, SpidersAndMites, Nematodes, Oomycetes, Phytoplasms, Plants).

![AGROSCOPE_OS_overview_rRNAcontent](./images/AGROSCOPE_OS_overview_rRNAcontent.png)

In the graph below, the same barplot is shown, but now without the categories "Plants" and "Other", showing a "zoomed-in" version of the previous barplot.

![AGROSCOPE_OS_overview_rRNAcontent_withoutplants](./images/AGROSCOPE_OS_overview_rRNAcontent_withoutplants.png)

### Results second phase: Detailed analysis of selected samples

Next, for each selected sample, we did a taxonomic classification of the reads by Kraken2, a k-mer based taxonomic classifier, against the non-redundant Nucleotide database of Genbank. **Krona plot can be downloaded [here](./images/KronaplotA2), and below you can find a summary of our findings.**

**We strongly recommend you to read more about the methods and interpretation [here](../../Report_interpretation.md#second-phase-detailed-taxonomic-classification) before you start looking at the plots.**

The detailed analysis consists of the following steps:
- quality control using [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) and (if necessary) adapter removal using [cutadapt](https://cutadapt.readthedocs.io/en/stable/)
- taxonomic assignment of reads using [Kraken2](https://github.com/DerrickWood/kraken2/wiki) against the Genbank non-redundant Nucleotide database
- visualisation of the classification using [Krona](https://github.com/marbl/Krona/wiki)

#### Agroscope_OS_Solanum_S1
A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotA2/AGROSCOPE_OS_151217_SND405_B_L008_HXU-2_Analysis2-kraken2-nr.html).

![AGROSCOPE_OS_Solanum_S1_Kronaplot_kraken2_overview](./images/AGROSCOPE_OS_Solanum_S1_Kronaplot_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group  | Taxon  | RPM  | Number of reads  |
|---|---|---|---|
| Fungi | *Rhizoctonia solani* | 1494 | 193431 |
| Fungi | *Colletotrichum spp.* | 238 | 30844 |
| Fungi | *Fusarium oxysporum* | 430 | 17485 |

![AGROSCOPE_OS_Solanum_S1_kraken2_fungi_rhizoctoniasolani](./images/AGROSCOPE_OS_Solanum_S1_kraken2_fungi_rhizoctoniasolani.png)

![AGROSCOPE_OS_Solanum_S1_kraken2_fungi_colletotrichum](./images/AGROSCOPE_OS_Solanum_S1_kraken2_fungi_colletotrichum.png)

![AGROSCOPE_OS_Solanum_S1_kraken2_fungi_fusariumoxysporum](./images/AGROSCOPE_OS_Solanum_S1_kraken2_fungi_fusariumoxysporum.png)
