## ILVO results

### Participant information

| Name             	| Email                 	| Institute 	| Country 	|
|------------------	|-----------------------	|-----------	|---------	|
| Yoika Foucart 	|  yoika.foucart@ilvo.vlaanderen.be	|  ILVO  	| Belgium 	|

### Sample info

| Sample name  | Original name  | Plant species  | Tissue | Viruses/viroids detected in previous analyses  | Read length  | Mapping method |
|---|---|---|---|---|---|---|
| ILVO_Stachys_S1  | ILVO_Stachysaffinis  | *Stachys affinis*  | leaves  | PhCMoV | 2 x 150bp  | BWA  |
| ILVO_Citrus_S2  |  ILVO_Citrussinensis | *Citrus sinensis*  | leaves  | CeVd,HSVd,CTV,CvdIII,CVEV | unknown  | BWA  |
| ILVO_Prunus_S3  | ILVO_Prunusavium  | *Prunus avium*  | leaves  | LChV1,CVA,PDV | unknown  | BWA  |
| ILVO_Ipomoea_S4  |  ILVO_Ipomoeabatatas1 | *Ipomoea batatas*  | leaves  | SPVG,SPV2,SPVC,SPFMV | unknown  |  BWA |
| ILVO_Solanum_S5  |  ILVO_Solanumlycopersicum1 | *Solanum lycopersicum*  | leaves  | unknown | unknown  | BWA  |
| ILVO_Solanum_S6  | ILVO_Solanumlycopersicum2  | *Solanum lycopersicum*  | leaves  | PepMV,STV | 2 x 150bp  | BWA  |
| ILVO_Solanum_S7  |ILVO_Solanumlycopersicum3  | *Solanum lycopersicum*  | leaves  | PepMV,STV,PVY | 2 x 150bp  | BWA  |
| ILVO_Capsicum_S8  | ILVO_Capsicumannuum1  | *Capsicum annuum*  | leaves  | BPEV,PepMV | 2 x 150bp  | BWA  |
| ILVO_Solanum_S9  | ILVO_Solanummelongena  | *Solanum melongena*  | leaves  | PepMV | 2 x 150bp  | BWA  |
| ILVO_Solanum_S10  | ILVO_Solanumlycopersicum4  | *Solanum lycopersicum*  | leaves  | ToCV,STV,PepMV  | 2 x 150bp  | BWA  |
| ILVO_Capsicum_S11  | ILVO_Capsicumannuum2  | *Capsicum annuum*  | leaves  | BPEV,PCV | 2 x 150bp  | BWA  |
| ILVO_Capsicum_S12  | ILVO_Capsicumannuum3  | *Capsicum annuum*  | leaves  | PCV | 2 x 150bp  | BWA  |
| ILVO_Ipomoea_S13  |  ILVO_Ipomoeabatatas2  | *Ipomoea batatas*  | leaves  | SPFMV,SPVG,SPVC,SPV2 | 2 x 150bp  | BWA  |
| ILVO_Oxalis_S14  | ILVO_Oxalistuberosa1  | *Oxalis tuberosa*  | tubers  | ArMV  | 2 x 150bp  | BWA  |
| ILVO_Oxalis_S15  | ILVO_Oxalistuberosa2  | *Oxalis tuberosa*  | tubers  | ArMV  | 2 x 150bp  | BWA  |
| ILVO_Smallanthus_S16  | ILVO_Smallanthussonchifolius  | *Smallanthus sonchifolius*  | leaves  |  YVA, CMV, YNMoV, PYV, FCiLV | 2 x 150bp  | BWA  |
| ILVO_Ipomoea_S17  | ILVO_Ipomoeabatatas3  | *Ipomoea batatas*  | leaves  | unknown | 2 x 150bp  | BWA  |
| ILVO_Dioscorea_S18  | ILVO_Dioscorea  | *Dioscorea* sp.  | leaves  | PYV,YYSMV,YMMV,YCNV,DBALV,DBRTV  | 2 x 150bp  | BWA  |
| ILVO_Malus_S19  | ILVO_Malusdomestica1  | *Malus domestica*  | pollen  |  AHVd | unknown  | BWA  |
| ILVO_Malus_S20  | ILVO_Malusdomestica2  | *Malus domestica*  | pollen  | AHVd,ASGV,CLVB | unknown  | BWA  |

### Results first phase: mapping against SILVA 132 LSU

The reads were mapped against the SILVA v132 LSU database as explained in the [instructions](https://gitlab.com/ilvo/PHBN-WP4-RNAseq_Community_Screening/-/tree/master/Instructions).
Mapping reports were sent to ILVO, and summarizing graphs were made.


For each organism group of interest, the number of mapped reads were summed and are shown in the barplot below. The percentage in the Y-axis points to the proportion of mapped reads as compared to the total number of mapped reads. You can hence interprete the complete bar as the total rRNA population of a sample, colored by organism category. The category "Other" points to reads which mapped to the rRNA database, but do not belong to the organism groups of interest (Bacteria, Fungi, Insects, SpidersAndMites, Nematodes, Oomycetes, Phytoplasms, Plants).

![ILVO_overview_rRNAcontent](./images/ILVO_overview_rRNAcontent.png)

In the graph below, the same barplot is shown, but now without the categories "Plants" and "Other", showing a "zoomed-in" version of the previous barplot.

![ILVO_overview_rRNAcontent_withoutplants](./images/ILVO_overview_rRNAcontent_withoutplants.png)

### Detailed analysis of selected samples

Next, for each selected sample, we did a taxonomic classification of the reads by Kraken2, a k-mer based taxonomic classifier, against the non-redundant Nucleotide database of Genbank. **Krona plots can be downloaded [here](./images/KronaplotsA2), and below you can find a summary of our findings.**

**We strongly recommend you to read more about the methods and interpretation [here](https://gitlab.com/ilvo/PHBN-WP4-RNAseq_Community_Screening/-/blob/master/Report_interpretation.md#second-phase-detailed-taxonomic-classification) before you start looking at the plots.**

The detailed analysis consists of the following steps:
- quality control using [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) and (if necessary) adapter removal using [cutadapt](https://cutadapt.readthedocs.io/en/stable/)
- taxonomic assignment of reads using [Kraken2](https://github.com/DerrickWood/kraken2/wiki) against the Genbank non-redundant Nucleotide database
- visualisation of the classification using [Krona](https://github.com/marbl/Krona/wiki)

#### ILVO_Stachys_S1

A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](link to be inserted once everything is on gitlab).

![ILVO_Stachys_S1_Kronaplot_kraken2_overview](./images/ILVO_Stachys_S1_Kronaplot_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group | Taxon                         	| RPM 	| number of reads	|
|-------|--------------------------------	|-----------	|-------------------	|
| Arachnida   |  *Tetranychus urticae*    |  760   | 26314
| Hexapoda  |  *Frankliniella occidentalis*    |  186   | 6444
| Fungi  |  *Botrytis cinerea*    |  131  | 4527
| Fungi  |  *Alternaria* spp.    |  317  | 10989

![ILVO_Stachys_S1_kraken2_Arachnida_Tetranychusurticae](./images/ILVO_Stachys_S1_kraken2_Arachnida_Tetranychusurticae.png)

![ILVO_Stachys_S1_kraken2_Hexapoda_Frankliniellaoccidentalis](./images/ILVO_Stachys_S1_kraken2_Hexapoda_Frankliniellaoccidentalis.png)

![ILVO_Stachys_S1_Fungi_Botrytiscinerea](./images/ILVO_Stachys_S1_Fungi_Botrytiscinerea.png)

![ILVO_Stachys_S1_kraken2_Fungi_Alternaria](./images/ILVO_Stachys_S1_kraken2_Fungi_Alternaria.png)

#### ILVO_Citrus_S2

A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](link to be inserted once everything is on gitlab).

![ILVO_Citrus_S2_kraken2_overview](./images/ILVO_Citrus_S2_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group | Taxon                         	| RPM	| number of reads	|
|-------|--------------------------------	|-----------	|-------------------	|
| Fungi  |  *Fusarium* spp.|  205    | 4401
| Fungi  |  *Alternaria* spp. |  169    | 3626
| Fungi  |  *Botrytis cinerea*  |  131    | 2799

![ILVO_Citrussinensis_kraken2_Fungi_Fusariumspp](./images/ILVO_Citrussinensis_kraken2_Fungi_Fusariumspp.png)

![ILVO_Citrussinensis_kraken2_Fungi_Alternariaspp](./images/ILVO_Citrussinensis_kraken2_Fungi_Alternariaspp.png)

![ILVO_Citrussinensis_kraken2_Fungi_Botrytiscinerea](./images/ILVO_Citrussinensis_kraken2_Fungi_Botrytiscinerea.png)

#### ILVO_Ipomoea_S4

A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](link to be inserted once everything is on gitlab).

![ILVO_Ipomea_S4_Kraken2_overview](./images/ILVO_Ipomea_S4_Kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group | Taxon                         	| RPM	| number of reads	|
|-------|--------------------------------	|-----------	|-------------------	|
| Fungi  |  *Alternaria* spp.|  261  | 379

![ILVO_Ipomea_S4_kraken2_Fungi_Alternariaspp](./images/ILVO_Ipomea_S4_kraken2_Fungi_Alternariaspp.png)

#### ILVO_Solanum_S5

A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](link to be inserted once everything is on gitlab).

![ILVO_Solanum_S5_kraken2_overview](./images/ILVO_Solanum_S5_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group | Taxon                         	| RPM 	| number of reads 	|
|-------|--------------------------------	|-----------	|-------------------	|
| Hexapoda  |  *Drosophila melanogaster*    | 2808 | 119791

![ILVO_Solanum_S5_kraken2_Hexapoda_Drosophilamelanogaster](./images/ILVO_Solanum_S5_kraken2_Hexapoda_Drosophilamelanogaster.png)

#### ILVO_Solanum_S7

A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](link to be inserted once everything is on gitlab).

![ILVO_Solanum_S7_kraken2_overview](./images/ILVO_Solanum_S7_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group | Taxon                         	| RPM	| number of reads	|
|-------|--------------------------------	|-----------	|-------------------	|
| Fungi  |  *Botrytis cinerea*    | 6442 | 173473
| Fungi  |  *Passalora fulva*    | 446 | 12000
| Fungi  |  *Erysiphaceae*    | 322| 8683

![ILVO_Solanum_S7_Kraken2_Fungi_Botrytiscinerea](./images/ILVO_Solanum_S7_Kraken2_Fungi_Botrytiscinerea.png)

![ILVO_Solanum_S7_Kraken2_Fungi_PassaloraFulva](./images/ILVO_Solanum_S7_Kraken2_Fungi_PassaloraFulva.png)

![ILVO_Solanum_S7_Kraken2_Fungi_Erysiphaceae](./images/ILVO_Solanum_S7_Kraken2_Fungi_Erysiphaceae.png)

#### ILVO_Capsicum_S8

A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](link to be inserted once everything is on gitlab).

![ILVO_Capsicum_S8_Kronaplot_contig_overview](./images/ILVO_Capsicum_S8_Kronaplot_contig_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group | Taxon                         	| RPM 	| number of reads	|
|-------|--------------------------------	|-----------	|-------------------	|
| Hexapoda  |  *Acyrthosiphon pisum*    | 32131 | 1227479
| Hexapoda  |  *Myzus persicae*    | 87467 | 3341448
| Fungi |  *Alternaria* spp.    | 127 | 4856

![ILVO_Capsicum_S8_kraken2_Hexapoda_Acyrthosiphonpisum](./images/ILVO_Capsicum_S8_kraken2_Hexapoda_Acyrthosiphonpisum.png)

![ILVO_Capsicum_S8_kraken2_Hexapoda_Myzuspersicae](./images/ILVO_Capsicum_S8_kraken2_Hexapoda_Myzuspersicae.png)

![ILVO_Capsicum_S8_kraken2_Fungi_Alternariaspp](./images/ILVO_Capsicum_S8_kraken2_Fungi_Alternariaspp.png)

#### ILVO_Solanum_S9

A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](link to be inserted once everything is on gitlab).

![ILVO_Solanum_S9_kraken2_overview](./images/ILVO_Solanum_S9_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group | Taxon                         	| RPM 	| number of reads	|
|-------|--------------------------------	|-----------	|-------------------	|
| Hexapoda  |  *Acyrthosiphon pisum*    | 23004 | 733027
| Hexapoda  |  *Myzus persicae*    | 104861 | 3341448
| Oomycetes  |  *Albugo* spp.   | 1086 | 34609
| Fungi  |  *Alternaria* spp.   | 119 | 3791
| Fungi  |  *Botrytis cinerea*    | 923 | 29424

![ILVO_Solanum_S9_kraken2_Acyrthosiphonpisum](./images/ILVO_Solanum_S9_kraken2_Acyrthosiphonpisum.png)

![ILVO_Solanum_S9_kraken2_Myzuspersicae](./images/ILVO_Solanum_S9_kraken2_Myzuspersicae.png)

![ILVO_Solanum_S9_kraken2_Oomycetes_Albugospp](./images/ILVO_Solanum_S9_kraken2_Oomycetes_Albugospp.png)

![ILVO_Solanum_S9_kraken2_Fungi_Alternariaspp](./images/ILVO_Solanum_S9_kraken2_Fungi_Alternariaspp.png)

![ILVO_Solanum_S9_kraken2_Fungi_Botrytiscinerea](./images/ILVO_Solanum_S9_kraken2_Fungi_Botrytiscinerea.png)

#### ILVO_Solanum_S10

A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](link to be inserted once everything is on gitlab)

![ILVO_Solanum_S10_kraken2_Kronaplot_overview](./images/ILVO_Solanum_S10_kraken2_Kronaplot_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group | Taxon                         	| RPM 	| number of reads	|
|-------|--------------------------------	|-----------	|-------------------	|
| Arachnida  |  *Eryophyidae*   | 2760 | 80573
| Fungi  |  *Erysiphaceae*    | 433 | 12653

![ILVO_Solanum_S10_kraken2_Arachnida_Eriophydiae](./images/ILVO_Solanum_S10_kraken2_Arachnida_Eriophydiae.png)

![ILVO_Solanum_S10_kraken2_Fungi_Erysiphaceae](./images/ILVO_Solanum_S10_kraken2_Fungi_Erysiphaceae.png)

#### ILVO_Capsicum_S11

A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](link to be inserted once everything is on gitlab)

![ILVO_Capsicum_S11_kraken2_overview](./images/ILVO_Capsicum_S11_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group | Taxon                         	| RPM 	| number of reads	|
|-------|--------------------------------	|-----------	|-------------------	|
| Hexapoda |  *Acyrthosiphon pisum*    | 835 | 26633
| Hexapoda |  *Thrips tabaci*   | 476 | 15176
| Hexapoda |  *Myzus persicae*    | 216 | 6877
| Arachnida |  *Tetranychidae*   | 430 | 13706
| Insects |  *Frankliniella occidentalis*    | 203 |6476

![ILVO_Capsicum_S11_kraken2_Hexapoda_Acyrthosiphonpisum](./images/ILVO_Capsicum_S11_kraken2_Hexapoda_Acyrthosiphonpisum.png)

![ILVO_Capsicum_S11_kraken2_Hexapoda_Thripstabaci](./images/ILVO_Capsicum_S11_kraken2_Hexapoda_Thripstabaci.png)

![ILVO_Capsicum_S11_kraken2_Hexapoda_Myzyspersicae](./images/ILVO_Capsicum_S11_kraken2_Hexapoda_Myzyspersicae.png)

![ILVO_Capsicum_S11_kraken2_Arachnida_Tetranychidae](./images/ILVO_Capsicum_S11_kraken2_Arachnida_Tetranychidae.png)

![ILVO_Capsicum_S11_kraken2_Insects_Frankliniella](./images/ILVO_Capsicum_S11_kraken2_Insects_Frankliniella.png)

#### ILVO_Ipomoea_S13

A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](link to be inserted once everything is on gitlab)

![ILVO_Ipomoea_S13_kraken2_overview](./images/ILVO_Ipomoea_S13_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group | Taxon                         	| RPM 	| number of reads	|
|-------|--------------------------------	|-----------	|-------------------	|
| Insects | *Frankliniella occidentalis*      | 667 |17398
| Fungi | *Botrytis cinerea*      | 201 |5240
| Fungi | *Fusarium* spp.     | 581 |15155

![ILVO_Ipomoea_S13_kraken2_Insects_Frankliniella](./images/ILVO_Ipomoea_S13_kraken2_Insects_Frankliniella.png)

![ILVO_Ipomoea_S13_kraken2_Fungi_Botrytiscinerea](./images/ILVO_Ipomoea_S13_kraken2_Fungi_Botrytiscinerea.png)

![ILVO_Ipomoea_S13_kraken2_Fungi_Fusarium](./images/ILVO_Ipomoea_S13_kraken2_Fungi_Fusarium.png)

#### ILVO_Oxalis_S14

A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](link to be inserted once everything is on gitlab)

![ILVO_Oxalis_S14_kraken2_overview](./images/ILVO_Oxalis_S14_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group | Taxon                         	| RPM 	| number of reads 	|
|-------|--------------------------------	|-----------	|-------------------	|
| Insects | *Drosophila* spp.   | 566 |14823
| Arachnida | *Tetranychus urticae*   | 105 |2742
| Insects | *Aphis gossypii*  | 230 |8381

![ILVO_Oxalis_S14_kraken2_Insects_Drosophila](./images/ILVO_Oxalis_S14_kraken2_Insects_Drosophila.png)

![ILVO_Oxalis_S14_kraken2_Arachnida_Tetranychus](./images/ILVO_Oxalis_S14_kraken2_Arachnida_Tetranychus.png)

![ILVO_Oxalis_S14_kraken2_Insects_Aphisgossypii](./images/ILVO_Oxalis_S14_kraken2_Insects_Aphisgossypii.png)
