## INRAE results

### Participant information

| Name             	| Email                 	| Institute 	| Country 	|
|------------------	|-----------------------	|-----------	|---------	|
| Marie Lefebvre 	| marie.lefebvre@inra.fr  	| INRAE   	| France 	|

### Sample info

| Sample name  | Original name| Plant species | Tissue  | Viruses/viroids detected in previous analyses  | Read length  | Mapping method  | Selected for detailed analysis? |
|---|---|---|---|---|---|---|---|
| INRAE_Fragaria_S1  |TT2016-28|*Fragaria* sp.| unknown  | unknown  | 2 x 150bp  | BWA | no |
| INRAE_Vitis_S2  |I77-old| *Vitis vinifera*  | rootstock  | GRSPaV, HSVd, BotVF   | 2 x 150bp  | BWA  | yes |
| INRAE_Vitis_S3  |I77-recent| *Vitis vinifera* | rootstock  | GRSPaV, HSVd, BotVF   | 2 x 150bp  | BWA  | yes |
| INRAE_Prunus_S4  |PR72d| *Prunus domestica*  | unknown  |PBNSPaV, PelPaV, HSVd| 2 x 150bp  | BWA  | yes |

### Results first phase: mapping against SILVA 132 LSU

The reads were mapped against the SILVA v132 LSU database as explained in the [instructions](../../Instructions).
Mapping reports were sent to ILVO, and summarizing graphs were made.

For each organism group of interest, the number of mapped reads were summed and are shown in the barplot below. The percentage in the Y-axis points to the proportion of mapped reads as compared to the total number of mapped reads. You can hence interprete the complete bar as the total rRNA population of a sample, colored by organism category. The category "Other" points to reads which mapped to the rRNA database, but do not belong to the organism groups of interest (Bacteria, Fungi, Insects, SpidersAndMites, Nematodes, Oomycetes, Phytoplasms, Plants).

![INRAE_overview_rRNAcontent](./images/INRAE_overview_rRNAcontent.png)

In the graph below, the same barplot is shown, but now without the categories "Plants" and "Other", showing a "zoomed-in" version of the previous barplot.

![INRAE_overview_rRNAcontent_withoutplants](./images/INRAE_overview_rRNAcontent_withoutplants.png)

### Results second phase: Detailed analysis of selected samples

Next, for each selected sample, we did a taxonomic classification of the reads by Kraken2, a k-mer based taxonomic classifier, against the non-redundant Nucleotide database of Genbank. **Krona plots can be downloaded [here](./images/KronaplotsA2), and below you can find a summary of our findings.**

**We strongly recommend you to read more about the methods and interpretation [here](../../Report_interpretation.md#second-phase-detailed-taxonomic-classification) before you start looking at the plots.**

The detailed analysis consists of the following steps:
- quality control using [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) and (if necessary) adapter removal using [cutadapt](https://cutadapt.readthedocs.io/en/stable/)
- taxonomic assignment of reads using [Kraken2](https://github.com/DerrickWood/kraken2/wiki) against the Genbank non-redundant Nucleotide database
- visualisation of the classification using [Krona](https://github.com/marbl/Krona/wiki)

#### INRAE_Vitis_S2

A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotsA2/INRAE_I77-old_Analysis2-kraken2-nr.html).

![INRAE_Vitis_S2_Kronaplot_kraken2_overview](./images/INRAE_Vitis_S2_Kronaplot_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group  | Taxon  | RPM | Number of reads  |
|---|---|---|---|
| Fungi | *Botrytis cinerea* | 84898 | 1374624 |

![INRAE_Vitis_S2_Kronaplot_kraken2_fungi_botrytiscinerea](./images/INRAE_Vitis_S2_Kronaplot_kraken2_fungi_botrytiscinerea.png)

#### INRAE_Vitis_S3

A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotsA2/INRAE_I77-recent_Analysis2-kraken2-nr.html).

![INRAE_Vitis_S3_Kronaplot_kraken2_overview](./images/INRAE_Vitis_S3_Kronaplot_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

|  Pathogen group | Taxon  | RPM  |  Number of reads |
|---|---|---|---|
| Fungi | *Botrytis cinerea* | 24342 | 299174 |

![INRAE_Vitis_S3_Kronaplot_kraken2_Botrytiscinerea](./images/INRAE_Vitis_S3_Kronaplot_kraken2_Botrytiscinerea.png)

#### INRAE_Prunus_S4

A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./Reports/INRAE/images/KronaplotsA2/INRAE_PR72d_Analysis2_kraken2-nr.html).

![INRAE_Prunus_S4_Kronaplot_kraken2_overview](./images/INRAE_Prunus_S4_Kronaplot_kraken2_overview.png)

#### There were no clear signs of non-viral plant pathogens.
