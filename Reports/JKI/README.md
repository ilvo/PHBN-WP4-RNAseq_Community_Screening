## JKI results

### Participant information

| Name             	| Email                 	| Institute 	| Country 	|
|------------------	|-----------------------	|-----------	|---------	|
| Yahya Gaafar 	| yahya.gaafar@julius-kuehn.de 	|  JKI | Germany 	|

### Sample info

| Sample name  | Original name  | Plant species  | Tissue | Viruses/viroids detected in previous analyses  | Read length  | Mapping method  | Selected for detailed analysis? |
|---|---|---|---|---|---|---|---|
| JKI_Beta_S1  | JKI_YG_AGZiebell_Sample1  | *Beta vulgaris*  | leaves  | BSBV, BCV2  | 2 x 150bp  | Geneious  | yes |
| JKI_Alliaria_S2  | JKI_YG_AGZiebell_Sample2  | *Alliaria petiolata*  | leaves  | TCV  | 2 x 150bp  | Geneious  | no |
| JKI_Vicia_S3  | JKI_YG_AGZiebell_Sample3  | *Vicia faba*  | leaves  | BLRV  | 2 x 150bp  | Geneious  | no |

### Results first phase: mapping against SILVA 132 LSU

The reads were mapped against the SILVA v132 LSU database as explained in the [instructions](../../Instructions).
Mapping reports were sent to ILVO, and summarizing graphs were made.


For each organism group of interest, the number of mapped reads were summed and are shown in the barplot below. The percentage in the Y-axis points to the proportion of mapped reads as compared to the total number of mapped reads. You can hence interprete the complete bar as the total rRNA population of a sample, colored by organism category. The category "Other" points to reads which mapped to the rRNA database, but do not belong to the organism groups of interest (Bacteria, Fungi, Insects, SpidersAndMites, Nematodes, Oomycetes, Phytoplasms, Plants).

![JKI_overview_rRNAcontent](./images/JKI_overview_rRNAcontent.png)

In the graph below, the same barplot is shown, but now without the categories "Plants" and "Other", showing a "zoomed-in" version of the previous barplot.

![JKI_overview_rRNAcontent_withoutplants](./images/JKI_overview_rRNAcontent_withoutplants.png)

### Results second phase: Detailed analysis of selected samples

Next, for each selected sample, we did a taxonomic classification of the reads by Kraken2, a k-mer based taxonomic classifier, against the non-redundant Nucleotide database of Genbank. **Krona plot can be downloaded [here](./images/KronaplotA2), and below you can find a summary of our findings.**

**We strongly recommend you to read more about the methods and interpretation [here](../../Report_interpretation.md#second-phase-detailed-taxonomic-classification) before you start looking at the plots.**

The detailed analysis consists of the following steps:
- quality control using [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) and (if necessary) adapter removal using [cutadapt](https://cutadapt.readthedocs.io/en/stable/)
- taxonomic assignment of reads using [Kraken2](https://github.com/DerrickWood/kraken2/wiki) against the Genbank non-redundant Nucleotide database
- visualisation of the classification using [Krona](https://github.com/marbl/Krona/wiki)

#### JKI_Beta_S1

A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotA2/JKI_Beta_S1_Analysis2-kraken2-nr.html).

![JKI_Beta_S1_Kronaplot_kraken2_overview](./images/JKI_Beta_S1_Kronaplot_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group  | Taxon  | RPM  | number of reads  |
|---|---|---|---|
| Bacteria | *Pantoea agglomerans* | 12325 | 60795 |
| Fungi | *Alternaria* spp. | 907 | 4474 |
| Fungi | *Verticilium* spp. | 170 | 839 |
| Fungi | *Stemphylium* spp. | 101 | 499 |
| Fungi | *Fusarium* spp. | 679 | 3351 |

![JKI_Beta_S1_Kronaplot_kraken2_bacteria_pantoeaagglomerans](./images/JKI_Beta_S1_Kronaplot_kraken2_bacteria_pantoeaagglomerans.png)

![JKI_Beta_S1_Kronaplot_kraken2_fungi_alternariasp](./images/JKI_Beta_S1_Kronaplot_kraken2_fungi_alternariasp.png)

![JKI_Beta_S1_Kronaplot_kraken2_fungi_verticiliumspp](./images/JKI_Beta_S1_Kronaplot_kraken2_fungi_verticiliumspp.png)

![JKI_Beta_S1_Kronaplot_kraken2_fungi_stemphyliumspp](./images/JKI_Beta_S1_Kronaplot_kraken2_fungi_stemphyliumspp.png)

![JKI_Beta_S1_Kronaplot_kraken2_fungi_fusariumsp](./images/JKI_Beta_S1_Kronaplot_kraken2_fungi_fusariumsp.png)
