## KIS results

### Participant information

| Name             	| Email                 	| Institute 	| Country 	|
|------------------	|-----------------------	|-----------	|---------	|
| Janja Lamovsek & Irena Mavric Plesko 	| Janja.Lamovsek@kis.si & Irena.MavricPlesko@kis.si 	| KIS  	| Slovenia 	|

### Sample info

| Sample name  | Original name  | Plant species  | Tissue  | Viruses/viroids detected in previous analyses  | Read length  | Mapping method  | Selected for detailed analysis? |
|---|---|---|---|---|---|---|---|
| KIS_Glycine_S1  | V-492/16  | *Glycine max*  | leaves  | none  | 2 x 150bp | unknown  | yes |
| KIS_Glycine_S2  | V-605/16  | *Glycine max*  | leaves  | none  | 2 x 150bp  | unknown  | yes |
| KIS_Rubus_S3  | Rubus-Koseze  | *Rubus* sp. | leaves  | none  | 2 x 150bp  | unknown  | no |
| KIS_Vitis_S4  | V-083/15  | *Vitis vinifera*  | leaves  | GPGV, GRSPaV, GYSVd-1, HSVd  | 2 x 150bp  | unknown  | no |
| KIS_Rubus_S5  | V-227/13  | *Rubus* sp.  | leaves  | new emaravirus  | 2 x 150bp  | unknown  |  no |
| KIS_Allium_S6  | garlic 1  | *Allium sativum*  | leaves  | none  | 2 x 150bp  | unknown  | yes |
| KIS_Rubus_S7  | V-253/14  | *Rubus* sp.  | leaves  | SLRSV  | 1 x 100bp  | unknown  | no |
| KIS_Rubus_S8  | V-255/14  | *Rubus* sp.  | leaves  | SLRSV  | 2 x 150bp  | unknown  | yes |

### Results first phase: mapping against SILVA 132 LSU

The reads were mapped against the SILVA v132 LSU database as explained in the [instructions](../../Instructions).
Mapping reports were sent to ILVO, and summarizing graphs were made.

For each organism group of interest, the number of mapped reads were summed and are shown in the barplot below. The percentage in the Y-axis points to the proportion of mapped reads as compared to the total number of mapped reads. You can hence interprete the complete bar as the total rRNA population of a sample, colored by organism category. The category "Other" points to reads which mapped to the rRNA database, but do not belong to the organism groups of interest (Bacteria, Fungi, Insects, SpidersAndMites, Nematodes, Oomycetes, Phytoplasms, Plants).

![KIS_overview_rRNAcontent](./images/KIS_overview_rRNAcontent.png)

In the graph below, the same barplot is shown, but now without the categories "Plants" and "Other", showing a "zoomed-in" version of the previous barplot.

![KIS_overview_rRNAcontent_withoutplants](./images/KIS_overview_rRNAcontent_withoutplants.png)

### Results second phase: Detailed analysis of selected samples

Next, for each selected sample, we did a taxonomic classification of the reads by Kraken2, a k-mer based taxonomic classifier, against the non-redundant Nucleotide database of Genbank. **Krona plots can be downloaded [here](./images/KronaplotsA2), and below you can find a summary of our findings.**

**We strongly recommend you to read more about the methods and interpretation [here](../../Report_interpretation.md#second-phase-detailed-taxonomic-classification) before you start looking at the plots.**

The detailed analysis consists of the following steps:
- quality control using [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) and (if necessary) adapter removal using [cutadapt](https://cutadapt.readthedocs.io/en/stable/)
- taxonomic assignment of reads using [Kraken2](https://github.com/DerrickWood/kraken2/wiki) against the Genbank non-redundant Nucleotide database
- visualisation of the classification using [Krona](https://github.com/marbl/Krona/wiki)

#### KIS_Glycine_S1
A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotsA2/Glycine_S1_Analysis2.html).

![KIS_Glycine_S1_Kronaplot_kraken2_overview](./images/KIS_Glycine_S1_Kronaplot_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table(s) below.

| Pathogen group  | Taxon  | RPM | Number of reads  |
|---|---|---|---|
| Oomycetes  | *Phytophthora spp.* |276 | 3988 |

![KIS_Glycine_S1_Kraken2_Oomycetes_Phytophthora](./images/KIS_Glycine_S1_Kraken2_Oomycetes_Phytophtora.png)

#### KIS_Glycine_S2
A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotsA2/Glycine_S2_Analysis2.html).

![KIS_Glycine_S2_Kronaplot_kraken2_overview](./images/KIS_Glycine_S2_Kronaplot_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table(s) below.

| Pathogen group  | Taxon  | RPM  | Number of reads  |
|---|---|---|---|
| Arachnida  | *Tetranychus urticae* |345|3846 |
| Oomycetes  | *Phytophthora* spp. |295| 3285 |

![KIS_Glycine_S2_kraken2_Fungi_Tetranychusurticae](./images/KIS_Glycine_S2_kraken2_Fungi_Tetranychusurticae.png)

![KIS_Glycine_S2_kraken2_Oomycetes_Phytophthora](./images/KIS_Glycine_S2_kraken2_Oomycetes_Phytophtora.png)

#### KIS_Allium_S6
A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotsA2/Allium_S6_Analysis2.html).

![KIS_Allium_S6_Kronaplot_kraken2_overview](./images/KIS_Allium_S6_Kronaplot_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table(s) below.

| Pathogen group  | Taxon  | RPM  | Number of reads  |
|---|---|---|---|
| Fungi  | *Fusarium spp.* |726 | 871 |

![KIS_Glycine_S6_kraken2_Fungi_Fusariumspp](./images/KIS_Glycine_S6_kraken2_Fungi_Fusariumspp.png)


#### KIS_Rubus_S8
A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotsA2/Rubus_S8_Analysis2.html).

![KIS_Rubus_S8_Kronaplot_kraken2_overview](./images/KIS_Rubus_S8_Kronaplot_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table(s) below.

| Pathogen group  | Taxon  | RPM  | Number of reads  |
|---|---|---|---|
| Hexapoda  | *Aphis gossypii* |3063 | 14599 |

![KIS_Rubus_S8_kraken2_Hexapoda_Aphisgossypiii](./images/KIS_Rubus_S8_kraken2_Hexapoda_Aphisgossypiii.png)
