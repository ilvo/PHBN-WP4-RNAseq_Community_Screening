## MPI_CP results

### Participant information

| Name             	| Email                 	| Institute 	| Country 	|
|------------------	|-----------------------	|-----------	|---------	|
|  Chandan Pal	| chandan_pal143@yahoo.com 	|   MPI 	| New Zealand 	|

### Sample info

| Sample name  | Original name  | Plant species  | Tissue  | Viruses/viroids detected in previous analyses  | Read length  | Mapping method  | Selected for detailed analysis? |
|---|---|---|---|---|---|---|---|
| MPI_CP_Persicaria_S1  | S1 | *Persicaria odorata*  | leaves  | unknown Carmovirus  |  2 x 250bp | BWA  | no |
| MPI_CP_Melicytus_S2  |  S2 | *Melicytus* sp.  |  leaves | unknown Caulomovirus  | 2 x 250bp  | BWA  | no |
|  MPI_CP_Melicytus_S3 | S3  | *Melicytus* sp.  | leaves  | unknown Caulomovirus  | 2 x 250bp  |  BWA | yes |
| MPI_CP_Melicytus_S4  | S4 | *Melicytus* sp.  | leaves  | unknown Caulomovirus  | 2 x 250bp  | BWA  | no |
| MPI_CP_Macropiper_S5  | S5  | *Macropiper excelsum*  | leaves  | none  | 2 x 250bp  | BWA  | yes |
| MPI_CP_Campanula_S6  |  S6 | *Campanula medium*  | leaves  | unknown  | 2 x 250bp  | BWA  | no |

### Results first phase: mapping against SILVA 132 LSU

The reads were mapped against the SILVA v132 LSU database as explained in the [instructions](../../Instructions).
Mapping reports were sent to ILVO, and summarizing graphs were made.


For each organism group of interest, the number of mapped reads were summed and are shown in the barplot below. The percentage in the Y-axis points to the proportion of mapped reads as compared to the total number of mapped reads. You can hence interprete the complete bar as the total rRNA population of a sample, colored by organism category. The category "Other" points to reads which mapped to the rRNA database, but do not belong to the organism groups of interest (Bacteria, Fungi, Insects, SpidersAndMites, Nematodes, Oomycetes, Phytoplasms, Plants).

![MPI_CP_overview_rRNAcontent](./images/MPI_CP_overview_rRNAcontent.png)

In the graph below, the same barplot is shown, but now without the categories "Plants" and "Other", showing a "zoomed-in" version of the previous barplot.

![MPI_CP_overview_rRNAcontent_withoutplants](./images/MPI_CP_overview_rRNAcontent_withoutplants.png)

### Results second phase: Detailed analysis of selected samples

Next, for each selected sample, we did a taxonomic classification of the reads by Kraken2, a k-mer based taxonomic classifier, against the non-redundant Nucleotide database of Genbank. **Krona plots can be downloaded [here](./images/KronaplotsA2), and below you can find a summary of our findings.**

**We strongly recommend you to read more about the methods and interpretation [here](../../Report_interpretation.md#second-phase-detailed-taxonomic-classification) before you start looking at the plots.**

The detailed analysis consists of the following steps:
- quality control using [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) and (if necessary) adapter removal using [cutadapt](https://cutadapt.readthedocs.io/en/stable/)
- taxonomic assignment of reads using [Kraken2](https://github.com/DerrickWood/kraken2/wiki) against the Genbank non-redundant Nucleotide database
- visualisation of the classification using [Krona](https://github.com/marbl/Krona/wiki)

#### MPI_CP_Melicytus_S3
A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotsA2/S3_Analysis2-kraken2-nr.html).

![MPI_CP_Melicytus_S3_Kronaplot_kraken2_overview](./images/MPI_CP_Melicytus_S3_Kronaplot_kraken2_overview.png)

#### There were no clear signs of non-viral plant pathogens.

#### MPI_CP_Macropiper_S5
A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotsA2/S5_Analysis2-kraken2-nr.html).

![MPI_CP_Macropiper_S5_Kronaplot_kraken2_overview](./images/MPI_CP_Macropiper_S5_Kronaplot_kraken2_overview.png)

#### There were no clear signs of non-viral plant pathogens.
