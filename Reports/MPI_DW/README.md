## MPI_DW results

### Participant information

| Name             	| Email                 	| Institute 	| Country 	|
|------------------	|-----------------------	|-----------	|---------	|
|  David Waite	| david.waite@mpi.govt.nz 	| MPI   	|  New Zealand	|

### Sample info

| Sample name  | Original name  |  Plant species | Tissue  | Viruses/viroids detected in previous analyses  | Read length  |  Mapping method | Selected for detailed analysis? |
|---|---|---|---|---|---|---|---|
| MPI_DW_Allium_S1  | MPI_DW_Allium1  | *Allium triquetrum*  | leaves  | unknown species of Carlavirus  | 2 x 250bp  | BWA  | no |
|  MPI_DW_Allium_S2 | MPI_DW_Allium2  |  *Allium triquetrum* | leaves  | unknown species of Carlavirus  | 2 x 250bp  | BWA  | yes |
| MPI_DW_Clematis_S3  | MPI_DW_Clematis1  |  *Clematis paniculata* | leaves  | none  | 2 x 250bp  | BWA  | yes |
| MPI_DW_Clivia_S4  | MPI_DW_Clivia1  |  *Clivia* sp. | leaves  |  SnVY, NLSYV, NeLV | 2 x 250bp  | BWA  | no |
| MPI_DW_Daphne_S5  | MPI_DW_Daphne1  | *Daphne odora*  |  leaves | Daphne virus Y, ButMV  | 2 x 250bp  | BWA  | no |
| MPI_DW_Hemerocallis_S6  | MPI_DW_Hemerocallis1  | *Hemerocallis* sp.  | leaves  | BYDV, undetermined Luteoviridae  |  2 x 250bp | BWA  | no |
| MPI_DW_Melicytus_S7  | MPI_DW_Melicytus1  | *Melicytus ramiflorus*  | leaves  | unknown  | 2 x 250bp  | BWA  | yes |
| MPI_DW_Nandina_S8  | MPI_DW_Nandina1  | *Nandina domestica*  | leaves  |  novel Citrivirus | 2 x 250bp  | BWA  | no |
|  MPI_DW_Persea_S9 |  MPI_DW_Persea1 | *Persea americana*  |  leaves |  Persea americana chrysovirus | 2 x 250bp  | BWA  | no |
|  MPI_DW_Pittosporum_S10 |  MPI_DW_Pittosporum1 | *Pittosporum crassifolium*  | leaves  | GFkV-like  | 2 x 250bp  | BWA  | no |
|  MPI_DW_Solanum_S11 |  MPI_DW_Potato1 | *Solanum tuberosum* | tubers  | PMTV  | 2 x 250bp  | BWA  | no |
| MPI_DW_Solanum_S12  | MPI_DW_Potato2  | *Solanum tuberosum*  | tubers  | PMTV  | 2 x 250bp  |  BWA | no |
|  MPI_DW_Prunus_S13 | MPI_DW_Prunus1  | *Prunus persica* | leaves  | PNRSV, SLRSV  | 2 x 250bp  | BWA  | no |
|  MPI_DW_Rosa_S14 | MPI_DW_Rosa1  | *Rosa rubiginosa*  | leaves  | Undetermined Secoviridae  | 2 x 250bp  | BWA  | no |
|  MPI_DW_Rubus_S15 |  MPI_DW_Rubus1 | *Rubus idaeus* | leaves  | none  | 2 x 250bp  | BWA | no |
|  MPI_DW_Rubus_S16 | MPI_DW_Rubus2  | *Rubus idaeus*  | leaves  | none  |  2 x 250bp | BWA  | no |
| MPI_DW_Viburnum_S17 | MPI_DW_Viburnum1  | *Viburnum* sp.  |  leaves | unknown species of Carlavirus  | 2 x 250bp  | BWA  | no |

### Results first phase: mapping against SILVA 132 LSU

The reads were mapped against the SILVA v132 LSU database as explained in the [instructions](../../Instructions).
Mapping reports were sent to ILVO, and summarizing graphs were made.


For each organism group of interest, the number of mapped reads were summed and are shown in the barplot below. The percentage in the Y-axis points to the proportion of mapped reads as compared to the total number of mapped reads. You can hence interprete the complete bar as the total rRNA population of a sample, colored by organism category. The category "Other" points to reads which mapped to the rRNA database, but do not belong to the organism groups of interest (Bacteria, Fungi, Insects, SpidersAndMites, Nematodes, Oomycetes, Phytoplasms, Plants).

![MPI_DW_overview_rRNAcontent](./images/MPI_DW_overview_rRNAcontent.png)

In the graph below, the same barplot is shown, but now without the categories "Plants" and "Other", showing a "zoomed-in" version of the previous barplot.

![MPI_DW_overview_rRNAcontent_withoutplants](./images/MPI_DW_overview_rRNAcontent_withoutplants.png)

### Results second phase: Detailed analysis of selected samples

Next, for each selected sample, we did a taxonomic classification of the reads by Kraken2, a k-mer based taxonomic classifier, against the non-redundant Nucleotide database of Genbank. **Krona plots can be downloaded [here](./images/KronaplotsA2), and below you can find a summary of our findings.**

**We strongly recommend you to read more about the methods and interpretation [here](../../Report_interpretation.md#second-phase-detailed-taxonomic-classification) before you start looking at the plots.**

The detailed analysis consists of the following steps:
- quality control using [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) and (if necessary) adapter removal using [cutadapt](https://cutadapt.readthedocs.io/en/stable/)
- taxonomic assignment of reads using [Kraken2](https://github.com/DerrickWood/kraken2/wiki) against the Genbank non-redundant Nucleotide database
- visualisation of the classification using [Krona](https://github.com/marbl/Krona/wiki)

#### MPI_DW_Allium2
A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotsA2/MPI_DW_Allium_S2_Analysis2-kraken2-nr.html).

![MPI_DW_Allium_S2_Kronaplot_kraken2_overview](./images/MPI_DW_Allium_S2_Kronaplot_kraken2_overview.png)

#### There were no clear signs of non-viral plant pathogens.

#### MPI_DW_Clematis_S3
A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotsA2/MPI_DW_Clematis_S3_Analysis2-kraken2-nr.html).

![MPI_DW_Clematis_S3_Kronaplot_kraken2_overview](./images/MPI_DW_Clematis_S3_Kronaplot_kraken2_overview.png)

#### There were no clear signs of non-viral plant pathogens.

#### MPI_DW_Melicytus_S7
A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotsA2/MPI_DW_Melicytus_S7_Analysis2-kraken2-nr.html).

![MPI_DW_Melicytus_S7_Kronaplot_kraken2_overview](./images/MPI_DW_Melicytus_S7_Kronaplot_kraken2_overview.png)

#### There were no clear signs of non-viral plant pathogens.
