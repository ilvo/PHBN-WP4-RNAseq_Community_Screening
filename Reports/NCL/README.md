## NCL results

### Participant information

| Name             	| Email                 	| Institute 	| Country 	|
|------------------	|-----------------------	|-----------	|---------	|
| Ferran Salavert Pamblanco 	| f.salavert2@newcastle.ac.uk 	|  NCL  	| United Kingdom 	|

### Sample info

| Sample name | Original name  | Plant species | Tissue | Viruses/viroids detected in previous analyses | Read length | Mapping method | Selected for detailed analysis? |
|---|---|---|---|---|---|---|---|
| NCL_Poaceae-pool_S1  | FerranCarrotWeed2_S22  | pool  | stem and leaves  | unknown  | 2 x 300bp  | BWA  | yes |

### Results first phase: mapping against SILVA 132 LSU

The reads were mapped against the SILVA v132 LSU database as explained in the [instructions](../../Instructions).
Mapping reports were sent to ILVO, and summarizing graphs were made.

For each organism group of interest, the number of mapped reads were summed and are shown in the barplot below. The percentage in the Y-axis points to the proportion of mapped reads as compared to the total number of mapped reads. You can hence interprete the complete bar as the total rRNA population of a sample, colored by organism category. The category "Other" points to reads which mapped to the rRNA database, but do not belong to the organism groups of interest (Bacteria, Fungi, Insects, SpidersAndMites, Nematodes, Oomycetes, Phytoplasms, Plants).

![NCL_overview_rRNAcontent](./images/NCL_overview_rRNAcontent.png)

In the graph below, the same barplot is shown, but now without the categories "Plants" and "Other", showing a "zoomed-in" version of the previous barplot.

![NCL_overview_rRNAcontent_withoutplants](./images/NCL_overview_rRNAcontent_withoutplants.png)

### Results second phase: Detailed analysis of selected samples

Next, for each selected sample, we did a taxonomic classification of the reads by Kraken2, a k-mer based taxonomic classifier, against the non-redundant Nucleotide database of Genbank. **Krona plot can be downloaded [here](./images/KronaplotA2), and below you can find a summary of our findings.**

**We strongly recommend you to read more about the methods and interpretation [here](../../Report_interpretation.md#second-phase-detailed-taxonomic-classification) before you start looking at the plots.**

The detailed analysis consists of the following steps:
- quality control using [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) and (if necessary) adapter removal using [cutadapt](https://cutadapt.readthedocs.io/en/stable/)
- taxonomic assignment of reads using [Kraken2](https://github.com/DerrickWood/kraken2/wiki) against the Genbank non-redundant Nucleotide database
- visualisation of the classification using [Krona](https://github.com/marbl/Krona/wiki)

#### NCL_Poaceae-pool_S1
A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotA2/FerranCarrotWeed2_S22_L001_Analysis2-kraken2-nr.html).

![NCL_Poaceae-pool_S1_Kronaplot_kraken2_overview](./images/NCL_Poaceae-pool_S1_Kronaplot_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group  | Taxon  | RPM  | Number of reads  |
|---|---|---|---|
| Oomycetes | *Albugo laibachii* |107450 | 17409 |
| Fungi  | Erysiphaceae  | 11581 | 18755  |
| Hexapoda | *Acyrthosiphon pisum* | 588 |953|
| Fungi | *Alternaria* spp.| 267 | 432 |
| Fungi | *Puccinia* spp.| 110 | 178 |

![NCL_Poaceae_pool_S1_kraken2_Oomycetes_Albugolaibachii](./images/NCL_Poaceae_pool_S1_kraken2_Oomycetes_Albugolaibachii.png)

![NCL_Poaceae-pool_S1_kraken2_fungi_erysiphaceae](./images/NCL_Poaceae-pool_S1_kraken2_fungi_erysiphaceae.png)

![NCL_Poaceae-pool_S1_kraken2_Hexapoda_Acyrthosiphonpisum](./images/NCL_Poaceae-pool_S1_kraken2_Hexapoda_Acyrthosiphonpisum.png)

![NCL_Poaceae_pool_S1_kraken2_Fungi_Alternariaspp](./images/NCL_Poaceae_pool_S1_kraken2_Fungi_Alternariaspp.png)

![NCL_Poaceae_pool_S1_kraken2_Fungi_Pucciniaspp](./images/NCL_Poaceae_pool_S1_kraken2_Fungi_Pucciniaspp.png)
