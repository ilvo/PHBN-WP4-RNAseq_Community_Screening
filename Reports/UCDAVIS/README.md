## UCDAVIS results

### Participant information

| Name             	| Email                 	| Institute 	| Country 	|
|------------------	|-----------------------	|-----------	|---------	|
| Kristian Stevens 	| kastevens@ucdavis.edu 	| UCDavis   	| CA, USA 	|

### Sample info

| Sample name | Original name  | Plant species | Tissue | Viruses/viroids detected in previous analyses | Read length | Mapping method | Selected for detailed analysis? |
|---|---|---|---|---|---|---|---|
| UCDAVIS_Prunus_S1  | Almond-FT357  | *Prunus dulcis*  | leaves  | none | 1 x 75bp  | BWA  | no |
| UCDAVIS_Malus_S2  | Apple-FT336  | *Malus domestica*  | leaves  | none | 1 x 75bp  | BWA  | no |
| UCDAVIS_Pyrus_S3  | Pear-FT115  | *Pyrus communis*  | leaves  | PBCVd  | 1 x 75bp  | BWA  | no |
| UCDAVIS_Pyrus_S4  | Pear-FT181  | *Pyrus communis*  | leaves  | none  | 1 x 75bp  | BWA  | no |
| UCDAVIS_Prunus_S5  | Prunus-FT288  | *Prunus* sp.  | leaves  | PBNSPaV  | 1 x 75bp  | BWA  | no |
| UCDAVIS_Prunus_S6  | Prunus-FT67  | *Prunus* sp.  | leaves  | PLMVd  | 1 x 75bp  | BWA  | no |
| UCDAVIS_Prunus_S7  | Cherry-FT212  | *Prunus cerasus*  | leaves  | NO-VLAs  | 1 x 75bp  | BWA  | yes |
| UCDAVIS_Rosa_S8  | Rose-R124 | *Rosa* sp.  | leaves | ApMV, RoCV-1, PNRSV, RSDaV  | 1 x 75bp  | BWA  | no |
| UCDAVIS_Rosa_S9  | Rose-R128  | *Rosa* sp.  | leaves | ApMV, PNRSV  | 1 x 75bp  | BWA  | no |
| UCDAVIS_Rosa_S10  | Rose-R68  | *Rosa* sp.  | leaves | none | 1 x 75bp  | BWA  | yes |
| UCDAVIS_Rosa_S11  | Rose-R233  | *Rosa* sp.  | leaves| RSDaV, RYVV  | 1 x 75bp  | BWA  | yes |
| UCDAVIS_Fragaria_S12  | Strawberry-St20  | *Fragaria* × *ananassa*  | leaves  | none | 1 x 75bp  | BWA  | no |
| UCDAVIS_Fragaria_S13  | Strawberry-St4  | *Fragaria* × *ananassa*  | leaves  | SVBV, SPaV, CuYV, BPYV  | 1 x 75bp  | BWA  | yes |
| UCDAVIS_Fragaria_S14  | Strawberry-St8  | *Fragaria* × *ananassa*  | leaves | CuYV, BPYV, SMoV, SPaV  | 1 x 75bp  | BWA  | no |
| UCDAVIS_Vitis_S15  | Grape-490  | *Vitis vinifera*  | leaves  | GLRaV-3, GVA, GAMaV, GYSVd-1, HSVd  | 1 x 75bp  | BWA  | no |
| UCDAVIS_Vitis_S16  | Grape-496  | *Vitis vinifera*   | leaves  | GRVFV, GLRaV-3, GVA, GVF, GYSVd-1, HSVd  | 1 x 75bp  | BWA  | no |
| UCDAVIS_Vitis_S17  | Grape-491  | *Vitis vinifera*   | leaves  | GLRaV-3, GVA, GFLV, GAMaV, GYSVd-1, HSVd  | 1 x 75bp  | BWA  | no |

### Results first phase: mapping against SILVA 132 LSU

The reads were mapped against the SILVA v132 LSU database as explained in the [instructions](../../Instructions).
Mapping reports were sent to ILVO, and summarizing graphs were made.

For each organism group of interest, the number of mapped reads were summed and are shown in the barplot below. The percentage in the Y-axis points to the proportion of mapped reads as compared to the total number of mapped reads. You can hence interprete the complete bar as the total rRNA population of a sample, colored by organism category. The category "Other" points to reads which mapped to the rRNA database, but do not belong to the organism groups of interest (Bacteria, Fungi, Insects, SpidersAndMites, Nematodes, Oomycetes, Phytoplasms, Plants).

![UCDAVIS_overview_new_rRNAcontent](./images/UCDAVIS_overview_rRNAcontent.png)

In the graph below, the same barplot is shown, but now without the categories "Plants" and "Other", showing a "zoomed-in" version of the previous barplot.

![UCDAVIS_overview_new_rRNAcontent_withoutplants](./images/UCDAVIS_overview_rRNAcontent_withoutplants.png)

### Results second phase: Detailed analysis of selected samples

Next, for each selected sample, we did a taxonomic classification of the reads by Kraken2, a k-mer based taxonomic classifier, against the non-redundant Nucleotide database of Genbank. **Krona plots can be downloaded [here](./images/KronaplotsA2), and below you can find a summary of our findings.**

**We strongly recommend you to read more about the methods and interpretation [here](../../Report_interpretation.md#second-phase-detailed-taxonomic-classification) before you start looking at the plots.**

The detailed analysis consists of the following steps:
- quality control using [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) and (if necessary) adapter removal using [cutadapt](https://cutadapt.readthedocs.io/en/stable/)
- taxonomic assignment of reads using [Kraken2](https://github.com/DerrickWood/kraken2/wiki) against the Genbank non-redundant Nucleotide database
- visualisation of the classification using [Krona](https://github.com/marbl/Krona/wiki)

#### UCDAVIS_Prunus_S7
A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotsA2/Cherry-FT212_S2_Analysis2-kraken2-nr.html).

![UCDAVIS_Prunus_S7_Kronaplot_kraken2_overview](./images/UCDAVIS_Prunus_S7_Kronaplot_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group  | Taxon  | RPM  | Number of reads  |
|---|---|---|---|
| Phytoplasma | *Candidatus Phytoplasma pruni* | 310 | 7420 |

![UCDAVIS_Prunus_S7_kraken2_phytoplasma_candidatusphytoplasmapruni](./images/UCDAVIS_Prunus_S7_kraken2_phytoplasma_candidatusphytoplasmapruni.png)

#### UCDAVIS_Rosa_S10
A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotsA2/Rose-R68_S6_Analysis2-kraken2-nr.html).

![UCDAVIS_Rosa_S10_Kronaplot_kraken2_overview](./images/UCDAVIS_Rosa_S10_Kronaplot_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group  | Taxon  | RPM  | Number of reads  |
|---|---|---|---|
| Arachnida | *Tetranychus urticae* | 2728 | 65860 |

![UCDAVIS_Rosa_S10_kraken2_arachnida_tetranychusurticae](./images/UCDAVIS_Rosa_S10_kraken2_arachnida_tetranychusurticae.png)

#### UCDAVIS_Rosa_S11
A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotsA2/Rose-R233_Analysis2-kraken2-nr.html).

![UCDAVIS_Rosa_S11_Kronaplot_kraken2_overview](./images/UCDAVIS_Rosa_S11_Kronaplot_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group  | Taxon  | RPM  | Number of reads  |
|---|---|---|---|
|Hexapoda | *Drosophila* spp. | 165| 3931 |

![UCDAVIS_Rosa_S10_kraken2_Hexapoda_Drosophilaspp](./images/UCDAVIS_Rosa_S10_kraken2_Hexapoda_Drosophilaspp.png)

#### UCDAVIS_Fragaria_S13
A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotsA2/Strawberry-St4_S3_Analysis2-kraken2-nr.html).

![UCDAVIS_Fragaria_S13_Kronaplot_kraken2_overview](./images/UCDAVIS_Fragaria_S13_Kronaplot_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group  | Taxon  | RPM  | Number of reads  |
|---|---|---|---|
|Hexapoda | *Frankliniella occidentalis* | 313 | 6922 |

![UCDAVIS_Fragaria_S13_kraken2_hexapoda_frankliniellaoccidentalis](./images/UCDAVIS_Fragaria_S13_kraken2_hexapoda_frankliniellaoccidentalis.png)
