## ULG results

### Participant information

| Name             	| Email                 	| Institute 	| Country 	|
|------------------	|-----------------------	|-----------	|---------	|
| Lucie Tamisier 	| lucie.tamisier@uliege.be 	|  ULG  	| Belgium 	|

### Sample info

| Sample name  | Original name  |Plant species  | Tissue  | Viruses/viroids detected in previous analyses  | Read length  | Mapping method  | Selected for detailed analysis? |
|---|---|---|---|---|---|---|---|
| ULG_Musa_S1  | ULG_LT_Banana1  | *Musa* sp.  | leaves  | BanMMV, BSV  | 2 x 76bp  | Geneious  | no |
| ULG_Musa_S2  | ULG_LT_Banana2 | *Musa* sp.  | leaves  | BanMMV, BSV, CMV, BBTV, BBrMV  | 2 x 76bp  | Geneious  | yes |
| ULG_Musa_S3  | ULG_LT_Banana3 | *Musa* sp.  | leaves  | BSV  | 2 x 76bp  | Geneious  | no |
| ULG_Morus_S4   | ULG_LT_BlackMulberry  |  *Morus nigra*  | leaves  | MLDV  | 2 x 150bp  | Geneious  | no |
| ULG_Prunus_S5  | ULG_LT_Cherry  | *Prunus avium*  | branches and leaves  | LChV-1, CVA, PDV  | 2 x 150bp  | Geneious  | no |
| ULG_Vitis_S6  | ULG_LT_Grapevine  | *Vitis vinifera*  | leaves  | GPGV, GSYVd, HSVd  | 2 x 150bp  | Geneious  | no |
| ULG_Nicotiana_S7  | ULG_LT_Nicotiana  | *Nicotiana benthamiana*  | leaves  | EWSMV  | 2 x 150bp  | Geneious  | no |

### Results first phase: mapping against SILVA 132 LSU

The reads were mapped against the SILVA v132 LSU database as explained in the [instructions](../../Instructions).
Mapping reports were sent to ILVO, and summarizing graphs were made.


For each organism group of interest, the number of mapped reads were summed and are shown in the barplot below. The percentage in the Y-axis points to the proportion of mapped reads as compared to the total number of mapped reads. You can hence interprete the complete bar as the total rRNA population of a sample, colored by organism category. The category "Other" points to reads which mapped to the rRNA database, but do not belong to the organism groups of interest (Bacteria, Fungi, Insects, SpidersAndMites, Nematodes, Oomycetes, Phytoplasms, Plants).

![ULG_overview_rRNAcontent](./images/ULG_overview_rRNAcontent.png)

In the graph below, the same barplot is shown, but now without the categories "Plants" and "Other", showing a "zoomed-in" version of the previous barplot.

![ULG_overview_rRNAcontent_withoutplants](./images/ULG_overview_rRNAcontent_withoutplants.png)

### Results second phase: Detailed analysis of selected samples

Next, for each selected sample, we did a taxonomic classification of the reads by Kraken2, a k-mer based taxonomic classifier, against the non-redundant Nucleotide database of Genbank. **Krona plot can be downloaded [here](./images/KronaplotA2), and below you can find a summary of our findings.**

**We strongly recommend you to read more about the methods and interpretation [here](../../Report_interpretation.md#second-phase-detailed-taxonomic-classification) before you start looking at the plots.**

The detailed analysis consists of the following steps:
- quality control using [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) and (if necessary) adapter removal using [cutadapt](https://cutadapt.readthedocs.io/en/stable/)
- taxonomic assignment of reads using [Kraken2](https://github.com/DerrickWood/kraken2/wiki) against the Genbank non-redundant Nucleotide database
- visualisation of the classification using [Krona](https://github.com/marbl/Krona/wiki)

#### ULG_Musa_S2
A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotA2/ULG_Musa_S2_kraken.html).

![ULG_Musa2_Kronaplot_kraken2_overview](./images/ULG_Musa2_Kronaplot_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group  | Taxon  | RPM  | Number of reads  |
|---|---|---|---|
| Arachnida | *Tetranychus urticae* |100| 889 |

![ULG_Musa_S2_kraken2_arachnida_tetranychusurticae](./images/ULG_Musa_S2_kraken2_arachnida_tetranychusurticae.png)
