## USDA results

### Participant information

| Name             	| Email                 	| Institute 	| Country 	|
|------------------	|-----------------------	|-----------	|---------	|
| Xiaojun Hu 	| xiaojun.hu@usda.gov 	|  USDA  	| USA 	|

### Sample info

| Sample name  | Original name  | Plant species  | Tissue  | Viruses/viroids detected in previous analyses  | Read length  | Mapping method  | Selected for detailed analysis?
|---|---|---|---|---|---|---|---|
| USDA_Malus_S1  | R33_A68 | *Malus domestica*  | leaves  | AHVd  | 1 x 75bp  | BWA  | no |
| USDA_Saccharum_S2  | R23_S41  | *Saccharum* sp.  | leaves  | SYLV  | 1 x 75bp  | BWA  | no |
| USDA_Malus_S3  | R14_A19  | *Malus domestica*  | leaves  | ASPV  | 1 x 75bp  | BWA | no |
| USDA_Saccharum_S4 | R7_S01  | *Saccharum* sp.  | leaves  | SSMV  | 1 x 75bp  | BWA  | no |
| USDA_Sorghum_S5  | R3_S36 | *Sorghum* sp.  | leaves  | SMV  | 1 x 75bp  | BWA  | no |
| USDA_Pyrus_S6  | R12_P08  | *Pyrus communis*  | leaves  | ASPV  | 1 x 75bp  | BWA  | yes |
| USDA_Pyrus_S7  | R12_P05 |  *Pyrus communis*  | leaves  | ASGV, ACLSV  | 1 x 75bp  | BWA | yes |
| USDA_Saccharum_S8  | R3_S02 | *Saccharum* sp.   | leaves  | SYLV  | 1 x 75bp  | BWA  | yes |

### Results first phase: mapping against SILVA 132 LSU

The reads were mapped against the SILVA v132 LSU database as explained in the [instructions](../../Instructions).
Mapping reports were sent to ILVO, and summarizing graphs were made.

For each organism group of interest, the number of mapped reads were summed and are shown in the barplot below. The percentage in the Y-axis points to the proportion of mapped reads as compared to the total number of mapped reads. You can hence interprete the complete bar as the total rRNA population of a sample, colored by organism category. The category "Other" points to reads which mapped to the rRNA database, but do not belong to the organism groups of interest (Bacteria, Fungi, Insects, SpidersAndMites, Nematodes, Oomycetes, Phytoplasms, Plants).

![USDA_overview_rRNAcontent](./images/USDA_overview_rRNAcontent.png)

In the graph below, the same barplot is shown, but now without the categories "Plants" and "Other", showing a "zoomed-in" version of the previous barplot.

![USDA_overview_rRNAcontent_withoutplants](./images/USDA_overview_rRNAcontent_withoutplants.png)

### Results second phase: Detailed analysis of selected samples

Next, for each selected sample, we did a taxonomic classification of the reads by Kraken2, a k-mer based taxonomic classifier, against the non-redundant Nucleotide database of Genbank. **Krona plots can be downloaded [here](./images/KronaplotsA2), and below you can find a summary of our findings.**

**We strongly recommend you to read more about the methods and interpretation [here](../../Report_interpretation.md#second-phase-detailed-taxonomic-classification) before you start looking at the plots.**

The detailed analysis consists of the following steps:
- quality control using [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) and (if necessary) adapter removal using [cutadapt](https://cutadapt.readthedocs.io/en/stable/)
- taxonomic assignment of reads using [Kraken2](https://github.com/DerrickWood/kraken2/wiki) against the Genbank non-redundant Nucleotide database
- visualisation of the classification using [Krona](https://github.com/marbl/Krona/wiki)

#### USDA_Pyrus_S6
A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotsA2/R12_P08_Analysis2-kraken2-nr.html).

![USDA_Pyrus_S6_kraken2_overview](./images/USDA_Pyrus_S6_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group  | Taxon  | RPM  | Number of reads  |
|---|---|---|---|
| Arachnida | *Tetranychus urticae*  | 1680 | 41918  |
| Hexapoda | Pseudococcidae  | 351 | 8770  |

![USDA_Pyrus_S6_kraken2_tetranychusurticae](./images/USDA_Pyrus_S6_kraken2_tetranychusurticae.png)

![USDA_Pyrus_S6_kraken2_Hexapoda_Pseudococcidae](./images/USDA_Pyrus_S6_kraken2_Hexapoda_Pseudococcidae.png)

#### USDA_Pyrus_S7
A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotsA2/R12_P05_Analysis2-kraken2-nr.html).

![USDA_Pyrus_S7_kraken2_overview](./images/USDA_Pyrus_S7_kraken2_overview.png)

Potentially interesting organisms as identified by checking the Krona plots and the organism lists are listed in the table below.

| Pathogen group  | Taxon  | RPM | Number of reads  |
|---|---|---|---|
| Arachnida | *Tetranychus urticae*  | 4244 | 118200 |

![USDA_Pyrus_S7_kraken2_tetranychusurticae](./images/USDA_Pyrus_S7_kraken2_tetranychusurticae.png)

#### USDA_Saccharum_S8
A screenshot of the resulting Krona file is pasted below. The interactive Krona plot (in html) can be downloaded [here](./images/KronaplotsA2/R3_S02_Analysis2-kraken2-nr.html).

![USDA_Saccharum_S8_kraken2_overview](./images/USDA_Saccharum_S8_kraken2_overview.png)

#### There were no clear signs of non-viral plant pathogens.
